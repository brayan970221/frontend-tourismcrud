import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TouristFormRoutingModule } from './tourist-form-routing.module';
import { TouristFormComponent } from './tourist-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TouristService } from 'src/app/services/tourist.service';


@NgModule({
  declarations: [TouristFormComponent],
  imports: [
    CommonModule,
    TouristFormRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [TouristService],
})
export class TouristFormModule { }
