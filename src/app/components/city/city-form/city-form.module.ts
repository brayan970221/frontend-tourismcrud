import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityFormRoutingModule } from './city-form-routing.module';
import { CityFormComponent } from './city-form.component';
import { CityService } from 'src/app/services/city.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [CityFormComponent],
  imports: [
    CommonModule,
    CityFormRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [CityService]
})
export class CityFormModule { }
