import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Tourist } from 'src/app/models/Tourist';
import { Response } from 'src/app/models/Response';

import { TouristListComponent } from './tourist-list.component';

describe('TouristListComponent', () => {
  let component: TouristListComponent;
  let fixture: ComponentFixture<TouristListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [TouristListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouristListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(window, 'confirm').and.returnValue(true);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ReadAllTourist test', () => {
    const res: Tourist[] = [{ id: '1' }, { id: '2' }];
    spyOn(component.touristService, 'readAllTourist').and.returnValue(of(res));
    component.readAllTourist();
    expect(component.error).toBeFalsy();
    expect(component.tourists).toEqual(res);
  });

  it('DeleteTourist test', () => {
    const res: Response = { msg: 'Deleted successfully' };
    spyOn(component.touristService, 'deleteTourist').and.returnValue(of(res));
    component.deleteTourist('1');
    expect(component.error).toBeFalsy();
    expect(component.deleteResponse).toEqual(res);
  });
});
