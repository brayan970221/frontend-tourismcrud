import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Travel } from 'src/app/models/Travel';
import { Response } from 'src/app/models/Response';

import { TravelListComponent } from './travel-list.component';

describe('TravelListComponent', () => {
  let component: TravelListComponent;
  let fixture: ComponentFixture<TravelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [TravelListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ReadAllTravels test', () => {
    const res: Travel[] = [{ id: 1 }, { id: 2 }];
    spyOn(component.travelService, 'readAllTravels').and.returnValue(of(res));
    component.readAllTravels();
    expect(component.error).toBeFalsy();
    expect(component.travels).toEqual(res);
  });

  it('DeleteTourist test', () => {
    const res: Response = { msg: 'Deleted successfully' };
    spyOn(component.travelService, 'deleteTravelById').and.returnValue(of(res));
    component.deleteTravel(1);
    expect(component.error).toBeFalsy();
    expect(component.deleteResponse).toEqual(res);
  });

  it('ChangeTouristFilter test', () => {
    component.filterForm.controls['idTourist'].setValue('123');
    component.onChangeTouristFilter();
    expect(component.touristFilterFlag).toBeTruthy();
    component.filterForm.controls['idTourist'].setValue('');
    component.onChangeTouristFilter();
    expect(component.touristFilterFlag).toBeFalsy();
  });

  it('ChangeCityFilter test', () => {
    component.filterForm.controls['cityName'].setValue('Bogota');
    component.onChangeCityFilter();
    expect(component.cityFilterFlag).toBeTruthy();
    component.filterForm.controls['cityName'].setValue('');
    component.onChangeCityFilter();
    expect(component.cityFilterFlag).toBeFalsy();
  });

  it('readTravelByIdTourist test', () => {
    const travel: Travel[] = [{ id: 1 }, { id: 2 }];
    spyOn(component.travelService, 'filterTravelsByTourist').and.returnValue(
      of(travel)
    );
    component.readTravelsByIdTourist('2');
    expect(component.travels).toEqual(travel);
  });

  it('readTravelByCity test', () => {
    const travel: Travel[] = [{ id: 1 }, { id: 2 }];
    spyOn(component.travelService, 'filterTravelsByCity').and.returnValue(
      of(travel)
    );
    component.readTravelsByCity('Bogota');
    expect(component.travels).toEqual(travel);
  });
});
