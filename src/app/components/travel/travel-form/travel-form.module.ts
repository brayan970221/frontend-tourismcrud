import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TravelFormRoutingModule } from './travel-form-routing.module';
import { TravelFormComponent } from './travel-form.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TravelService } from 'src/app/services/travel.service';


@NgModule({
  declarations: [TravelFormComponent],
  imports: [
    CommonModule,
    TravelFormRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [TravelService],
})
export class TravelFormModule { }
