import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NavigationComponent } from '../../navigation/navigation.component';
import { TravelListComponent } from '../travel-list/travel-list.component';
import { of } from 'rxjs';
import { Travel } from 'src/app/models/Travel';
import { City } from 'src/app/models/City';
import { Response } from 'src/app/models/Response';

import { TravelFormComponent } from './travel-form.component';

describe('TravelFormComponent', () => {
  let component: TravelFormComponent;
  let fixture: ComponentFixture<TravelFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'Travel',
            component: TravelListComponent,
          },
        ]),
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [TravelFormComponent, NavigationComponent],
      providers: [
        TravelFormComponent,
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { params: { id: '1' } } },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TravelFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('editContext', () => {
    const city: City[] = [{ id: 1 }, { id: 2 }];
    const travel: Travel = {
      id: 1,
      idCity: 1,
      cityName: 'city',
      idTourist: '1',
      travelDate: '01/01/2000',
    };
    spyOn(component.cityService, 'readAllCities').and.returnValue(of(city));
    spyOn(component.travelService, 'readTravelById').and.returnValue(
      of(travel)
    );
    component.ngOnInit();
    expect(component.editFlag).toBeTruthy();
  });

  it('creation success', () => {
    const res: Response = { msg: 'Created successfully' };
    spyOn(component.travelService, 'createTravel').and.returnValue(of(res));
    component.createTravel();
    expect(component.responseCU).toEqual(res);
  });

  it('creation failed', () => {
    const res: Response = { msg: 'Another msg' };
    spyOn(component.travelService, 'createTravel').and.returnValue(of(res));
    spyOn(window, 'alert');
    component.createTravel();
    expect(window.alert).toHaveBeenCalledWith(res.msg);
  });

  it('edit success', () => {
    const res: Response = { msg: 'Updated successfully' };
    spyOn(component.travelService, 'editTravel').and.returnValue(of(res));
    component.editTravel();
    expect(component.responseCU).toEqual(res);
  });

  it('edit failed', () => {
    const res: Response = { msg: 'Another msg' };
    spyOn(component.travelService, 'editTravel').and.returnValue(of(res));
    spyOn(window, 'alert');
    component.editTravel();
    expect(window.alert).toHaveBeenCalledWith(res.msg);
  });

  it('Valid form', () => {
    component.travelForm.controls['id'].setValue(2);
    component.travelForm.controls['idCity'].setValue(2);
    component.travelForm.controls['cityName'].setValue('city');
    component.travelForm.controls['idTourist'].setValue('1');
    component.travelForm.controls['travelDate'].setValue('01/01/2001');
    component.onSubmit();
    expect(component.travelForm.valid).toBeTruthy();
    expect(component.submitFlag).toBeFalsy();
  });

  it('invalid form', () => {
    component.travelForm.controls['id'].setValue(2);
    component.travelForm.controls['idCity'].setValue(null);
    component.travelForm.controls['cityName'].setValue(null);
    component.travelForm.controls['idTourist'].setValue(null);
    component.travelForm.controls['travelDate'].setValue(null);
    // Onlye verify required validators
    component.onSubmit();
    expect(component.travelForm.valid).toBeFalsy();
    expect(component.submitFlag).toBeTruthy();
  });

  it('Update city', () => {
    const idCity = '1';
    const cityName = 'Bogota';
    component.travelForm.controls['cityName'].setValue(idCity + '-' + cityName);
    component.updateIdCity();
    expect(component.city).toEqual(
      component.travelForm.controls['cityName'].value
    );
    expect(component.travelForm.controls['idCity'].value).toEqual(idCity);
  });
});
