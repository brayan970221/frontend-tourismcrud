import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/index',
    pathMatch: 'full',
  },
  {
    path: 'index',
    loadChildren: () => import('./components/main/main.module').then(m => m.MainModule),
  },
  {
    path: 'City',
    loadChildren: () => import('./components/city/city-list/city-list.module').then(m => m.CityListModule),
  },
  {
    path: 'City/create',
    loadChildren: () => import('./components/city/city-form/city-form.module').then(m => m.CityFormModule),
  },
  {
    path: 'City/edit/:id',
    loadChildren: () => import('./components/city/city-form/city-form.module').then(m => m.CityFormModule),
  },
  {
    path: 'Tourist',
    loadChildren: () => import('./components/tourist/tourist-list/tourist-list.module').then(m => m.TouristListModule),
  },
  {
    path: 'Tourist/edit/:id',
    loadChildren: () => import('./components/tourist/tourist-form/tourist-form.module').then(m => m.TouristFormModule),
  },
  {
    path: 'Tourist/create',
    loadChildren: () => import('./components/tourist/tourist-form/tourist-form.module').then(m => m.TouristFormModule),
  },
  {
    path: 'Travel',
    loadChildren: () => import('./components/travel/travel-list/travel-list.module').then(m => m.TravelListModule),
  },
  {
    path: 'Travel/create',
    loadChildren: () => import('./components/travel/travel-form/travel-form.module').then(m => m.TravelFormModule),
  },
  {
    path: 'Travel/edit/:id',
    loadChildren: () => import('./components/travel/travel-form/travel-form.module').then(m => m.TravelFormModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
