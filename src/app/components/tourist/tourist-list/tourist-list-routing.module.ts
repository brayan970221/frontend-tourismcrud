import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TouristListComponent } from './tourist-list.component';

const routes: Routes = [
  {
    path: '',
    component: TouristListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TouristListRoutingModule { }
