import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TravelListRoutingModule } from './travel-list-routing.module';
import { TravelListComponent } from './travel-list.component';
import { TravelService } from 'src/app/services/travel.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [TravelListComponent],
  imports: [
    CommonModule,
    TravelListRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [TravelService],
})
export class TravelListModule { }
