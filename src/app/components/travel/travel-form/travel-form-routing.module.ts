import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TravelFormComponent } from './travel-form.component';

const routes: Routes = [
  {
    path: '',
    component: TravelFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelFormRoutingModule { }
