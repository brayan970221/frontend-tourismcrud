import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { City } from 'src/app/models/City';
import { Response } from 'src/app/models/Response';
import { Tourist } from 'src/app/models/Tourist';
import { NavigationComponent } from '../../navigation/navigation.component';
import { TouristListComponent } from '../tourist-list/tourist-list.component';
import { TouristFormComponent } from './tourist-form.component';

describe('TouristFormComponent', () => {
  let component: TouristFormComponent;
  let fixture: ComponentFixture<TouristFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          {
            path: 'Tourist',
            component: TouristListComponent,
          },
        ]),
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [TouristFormComponent, NavigationComponent],
      providers: [
        TouristFormComponent,
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { params: { id: '1' } } },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouristFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('EditContext', () => {
    const city: City[] = [{ id: 1 }, { id: 2 }];
    const tourist: Tourist = {
      id: '1',
      name: 'name',
      idType: 'CC',
      destination: 'dest',
      travelBudget: 500,
      travelFrecuency: 2,
      birthDate: '01/01/1997',
      gender: 'M',
      creditCard: false,
    };
    spyOn(component.cityService, 'readAllCities').and.returnValue(of(city));
    spyOn(component.touristService, 'readTouristById').and.returnValue(
      of(tourist)
    );
    component.ngOnInit();
    expect(component.editFlag).toBeTruthy();
  });

  it('creation success', () => {
    const res: Response = { msg: 'Created successfully' };
    spyOn(component.touristService, 'createTourist').and.returnValue(of(res));
    component.createTourist();
    expect(component.responseCU).toEqual(res);
  });
  it('creation failed', () => {
    const res: Response = { msg: 'Another msg' };
    spyOn(component.touristService, 'createTourist').and.returnValue(of(res));
    spyOn(window, 'alert');
    component.createTourist();
    expect(window.alert).toHaveBeenCalledWith(res.msg);
  });
  it('edit success', () => {
    const res: Response = { msg: 'Updated successfully' };
    spyOn(component.touristService, 'editTourist').and.returnValue(of(res));
    component.editTourist();
    expect(component.responseCU).toEqual(res);
  });
  it('edit failed', () => {
    const res: Response = { msg: 'Another msg' };
    spyOn(component.touristService, 'editTourist').and.returnValue(of(res));
    spyOn(window, 'alert');
    component.editTourist();
    expect(window.alert).toHaveBeenCalledWith(res.msg);
  });
  it('Valid form', () => {
    component.touristForm.controls['id'].setValue('3');
    component.touristForm.controls['name'].setValue('Name');
    component.touristForm.controls['idType'].setValue('CC');
    component.touristForm.controls['birthDate'].setValue('01/01/01');
    component.touristForm.controls['gender'].setValue('M');
    component.touristForm.controls['travelFrecuency'].setValue(2);
    component.touristForm.controls['travelBudget'].setValue(200);
    component.touristForm.controls['destination'].setValue('Bogota');
    component.touristForm.controls['creditCard'].setValue(true);
    component.onSubmit();
    expect(component.touristForm.valid).toBeTruthy();
    expect(component.submitFlag).toBeFalsy();
  });
  it('invalid form', () => {
    component.touristForm.controls['id'].setValue('3');
    component.touristForm.controls['name'].setValue('Name');
    component.touristForm.controls['idType'].setValue('CC');
    component.touristForm.controls['birthDate'].setValue('01/01/01');
    component.touristForm.controls['gender'].setValue('M');
    component.touristForm.controls['travelFrecuency'].setValue(200);
    component.touristForm.controls['travelBudget'].setValue(-200);
    component.touristForm.controls['destination'].setValue('Bogota');
    component.touristForm.controls['creditCard'].setValue(true);
    component.onSubmit();
    expect(component.touristForm.valid).toBeFalsy();
    expect(component.submitFlag).toBeTruthy();
  });
});
