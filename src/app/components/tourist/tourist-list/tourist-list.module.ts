import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TouristListRoutingModule } from './tourist-list-routing.module';
import { TouristListComponent } from './tourist-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TouristService } from 'src/app/services/tourist.service';


@NgModule({
  declarations: [TouristListComponent],
  imports: [
    CommonModule,
    TouristListRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [TouristService],
})
export class TouristListModule { }
