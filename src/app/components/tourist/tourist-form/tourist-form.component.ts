import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tourist } from 'src/app/models/Tourist';
import { TouristService } from '../../../services/tourist.service';
import { CityService } from '../../../services/city.service';
import { FormBuilder, Validators } from '@angular/forms';
import { City } from 'src/app/models/City';
import { Response } from 'src/app/models/Response';

@Component({
  selector: 'app-tourist-form',
  templateUrl: './tourist-form.component.html',
})
export class TouristFormComponent implements OnInit {
  tourist: Tourist = {
    id: '0',
  };
  editFlag = false;
  cities: City[] = [];
  submitFlag = false;
  responseCU: Response = {};

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public touristService: TouristService,
    public cityService: CityService,
    public fb: FormBuilder,
    private ngZone: NgZone
  ) {}

  touristForm = this.fb.group({
    id: [null, [Validators.required, Validators.min(1)]],
    name: [null, Validators.required],
    idType: ['CC', Validators.required],
    birthDate: [null, Validators.required],
    gender: ['M', Validators.required],
    travelFrecuency: [
      null,
      [Validators.required, Validators.min(1), Validators.max(36)],
    ],
    travelBudget: [null, [Validators.required, Validators.min(0)]],
    destination: [null, Validators.required],
    creditCard: [false],
  });

  ngOnInit(): void {
    this.cityService
      .readAllCities()
      .subscribe((res) => (this.cities = res as City[]));
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.touristService.readTouristById(params.id).subscribe((res) => {
        this.touristForm.setValue(res);
        this.editFlag = true;
      });
    }
  }

  /**
   * Use the tourist service for create a tourist in the database
   * with the current local tourist object
   */
  createTourist(): void {
    this.touristService.createTourist(this.tourist).subscribe((res) => {
      this.responseCU = res as Response;
      if (this.responseCU.msg == 'Created successfully') {
        this.ngZone.run(() => this.router.navigate(['/Tourist']));
      } else {
        alert(res.msg);
      }
    });
  }

  /**
   * Use the tourist service for edit a tourist in the database with the
   * current local tourist object
   */
  editTourist(): void {
    this.touristService.editTourist(this.tourist).subscribe((res) => {
      this.responseCU = res as Response;
      if (this.responseCU.msg == 'Updated successfully') {
        this.editFlag = false;
        this.ngZone.run(() => this.router.navigate(['/Tourist']));
      } else {
        alert(res.msg);
      }
    });
  }

  /**
   * Verify the form, if is correct call the create/edit
   */
  onSubmit(): void {
    if (this.touristForm.valid) {
      this.submitFlag = false;
      this.tourist = this.touristForm.value;
      this.editFlag ? this.editTourist() : this.createTourist();
    } else {
      this.submitFlag = true;
    }
  }
}
