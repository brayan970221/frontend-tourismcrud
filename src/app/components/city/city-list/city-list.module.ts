import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityListRoutingModule } from './city-list-routing.module';
import { CityListComponent } from './city-list.component';
import { CityService } from 'src/app/services/city.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CityListComponent],
  imports: [
    CommonModule,
    CityListRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [CityService]
})
export class CityListModule { }
